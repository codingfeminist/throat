"""Peewee migrations -- 001_modmail.py.

Some examples (model - class or model name)::

    > Model = migrator.orm['model_name']            # Return model in current state by name

    > migrator.sql(sql)                             # Run custom SQL
    > migrator.python(func, *args, **kwargs)        # Run python code
    > migrator.create_model(Model)                  # Create a model (could be used as decorator)
    > migrator.remove_model(model, cascade=True)    # Remove a model
    > migrator.add_fields(model, **fields)          # Add fields to a model
    > migrator.change_fields(model, **fields)       # Change fields
    > migrator.remove_fields(model, *field_names, cascade=True)
    > migrator.rename_field(model, old_field_name, new_field_name)
    > migrator.rename_table(model, new_table_name)
    > migrator.add_index(model, *col_names, unique=False)
    > migrator.drop_index(model, *col_names)
    > migrator.add_not_null(model, *field_names)
    > migrator.drop_not_null(model, *field_names)
    > migrator.add_default(model, field_name, default)

"""

import datetime as dt
from enum import IntEnum
import peewee as pw

try:
    import playhouse.postgres_ext as pw_pext
except ImportError:
    pass

SQL = pw.SQL


class MessageMailbox(IntEnum):
    """Mailboxes for private messages."""

    INBOX = 200
    SENT = 201
    SAVED = 202
    ARCHIVED = 203  # Modmail only.
    TRASH = 204
    DELETED = 205


# Only need enough fields to make it generate the right SQL.
class User(pw.Model):
    uid = pw.CharField(primary_key=True, max_length=40)

    class Meta:
        table_name = "user"


class Message(pw.Model):
    mid = pw.PrimaryKeyField()

    class Meta:
        table_name = "message"



def migrate(migrator, database, fake=False, **kwargs):
    """Write your migrations here."""

    @migrator.create_model
    class SubMessageMailbox(pw.Model):
        mid = pw.ForeignKeyField(db_column="mid", model=Message, field="mid")
        mailbox = pw.IntegerField(default=MessageMailbox.INBOX)

        class Meta:
            table_name = "sub_message_mailbox"

    @migrator.create_model
    class SubMessageLog(pw.Model):
        mid = pw.ForeignKeyField(db_column="mid", model=Message, field="mid")
        uid = pw.ForeignKeyField(db_column="uid", model=User, field="uid")
        mailbox = pw.IntegerField()
        updated = pw.DateTimeField(default=dt.datetime.now)

        class Meta:
            table_name = "sub_message_log"

    # This migrator.create_model decorator has no effect on the
    # database since SiteMetadata already exists, but it adds the
    # model to the migrator so it can be used with the migrator's
    # database connection.
    @migrator.create_model
    class SiteMetadata(pw.Model):
        key = pw.CharField(null=True)
        value = pw.CharField(null=True)
        xid = pw.PrimaryKeyField()

        class Meta:
            table_name = "site_metadata"

    if not fake:
        try:
            SiteMetadata.get(SiteMetadata.key == "site.enable_modmail")
        except SiteMetadata.DoesNotExist:
            SiteMetadata.create(key="site.enable_modmail", value="1")


def rollback(migrator, database, fake=False, **kwargs):
    """Write your rollback migrations here."""
    migrator.remove_model("sub_message_mailbox")
    migrator.remove_model("sub_message_log")

    if not fake:
        SiteMetadata = migrator.orm["site_metadata"]
        SiteMetadata.delete().where(SiteMetadata.key == "site.enable_modmail").execute()
