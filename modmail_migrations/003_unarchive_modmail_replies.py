"""Peewee migrations -- 003_unarchive_modmail_replies.py.

Move archived modmail threads which have a new message
from a user which arrived after the message was archived to
the moderator inbox.

This migration must be run before 042_message_threads.py.

"""

import peewee as pw


def migrate(migrator, database, fake=False, **kwargs):
    """Write your migrations here."""
    SubMessageLog = migrator.orm["sub_message_log"]
    SubMessageMailbox = migrator.orm["sub_message_mailbox"]
    USER_TO_MODS = 101
    INBOX = 200
    ARCHIVED = 203

    # This migrator.create_model decorator has no effect on the
    # database since Message already exists, but it adds the
    # model to the migrator so it can be used with the migrator's
    # database connection.
    @migrator.create_model
    class Message(pw.Model):
        mid = pw.PrimaryKeyField()
        mtype = pw.IntegerField(null=True)
        reply_to = pw.ForeignKeyField(
            db_column="reply_to", null=True, model="self", field="mid"
        )
        posted = pw.DateTimeField(null=True)

        class Meta:
            table_name = "message"

    if not fake:
        # There was a bug in throat-be which would put message mids
        # of replies in the sub_message_log.  Fix those entries to use
        # the mid of the original message.
        logs = (
            SubMessageLog.select(SubMessageLog.id, SubMessageLog.mid, Message.reply_to)
            .join(Message, on=(SubMessageLog.mid == Message.mid))
            .where(Message.reply_to.is_null(False))
            .dicts()
        )

        for log in logs:
            SubMessageLog.update(mid=log["reply_to"]).where(
                SubMessageLog.id == log["id"]
            ).execute()

        # Find archived modmail conversations where the newest message
        # in the thread is from a user, and where the date of the
        # newest message is newer than when the thread was archived,
        # and unarchive those conversations.
        MessageAlias1 = Message.alias()
        conversation = MessageAlias1.select(
            pw.Case(
                None,
                [(MessageAlias1.reply_to.is_null(), MessageAlias1.mid)],
                MessageAlias1.reply_to,
            ).alias("convo_mid"),
        )
        MessageAlias2 = Message.alias()
        conversation_newest = (
            MessageAlias2.select(
                conversation.c.convo_mid,
                pw.fn.MAX(MessageAlias2.posted).alias("maxtime"),
            )
            .join(
                conversation,
                on=(conversation.c.convo_mid == MessageAlias2.mid)
                | (conversation.c.convo_mid == MessageAlias2.reply_to),
            )
            .group_by(conversation.c.convo_mid)
        )
        log_entry_newest = SubMessageLog.select(
            SubMessageLog.mid,
            pw.fn.MAX(SubMessageLog.updated).alias("most_recent_update"),
        ).group_by(SubMessageLog.mid)

        threads = (
            Message.select(conversation_newest.c.convo_mid)
            .join(
                conversation_newest,
                on=(
                    (
                        (conversation_newest.c.convo_mid == Message.mid)
                        | (conversation_newest.c.convo_mid == Message.reply_to)
                    )
                    & (conversation_newest.c.maxtime == Message.posted)
                ),
            )
            .switch(Message)
            .join(
                SubMessageMailbox,
                on=(SubMessageMailbox.mid == conversation_newest.c.convo_mid),
            )
            .join(
                log_entry_newest,
                on=((log_entry_newest.c.mid == conversation_newest.c.convo_mid)),
            )
            .where(
                (Message.mtype == USER_TO_MODS)
                & (SubMessageMailbox.mailbox == ARCHIVED)
                & (Message.posted > log_entry_newest.c.most_recent_update)
            )
            .dicts()
        )
        SubMessageMailbox.update(mailbox=INBOX).where(
            SubMessageMailbox.mid << [t["convo_mid"] for t in threads]
        ).execute()


def rollback(migrator, database, fake=False, **kwargs):
    """Write your rollback migrations here."""
    pass
